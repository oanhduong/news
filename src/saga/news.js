import {NewsActionTypes, appendNewsFn} from '../reducers/news';
import {takeEvery, call, put, fork, all} from 'redux-saga/effects';
import {getNews} from '../services/news';

const loadNewsWatcher = function* () {
  yield takeEvery(NewsActionTypes.LOAD_NEWS, function* ({payload: {page, pageSize, search}}) {
    try {
      const news = yield call(getNews, page, pageSize, search);
      yield put(appendNewsFn({page, pageSize, search, news}));
    } catch (e) {
      alert('Loading news error');
    }
  });
};

export default function* saga() {
  yield all([
    fork(loadNewsWatcher)
  ]);
}

