import './App.css';
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import { Navigation } from './links';
import { Provider } from 'react-redux';
import { store, history } from './store';

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Switch>
            {
              Navigation.map((route, index) => {
                return <Route exact key={index} path={route.to} render={() => route.component}></Route>;
              })
            }
          </Switch>
        </ConnectedRouter>
      </Provider>
    </div>
  );
}

export default App;

