import React from 'react';
import NewsPage from './pages/news';
import RegionsPage from './pages/regions';
import VideoPage from './pages/video';
import TvPage from './pages/tv';

export const Navigation = [
  {
    to: '/',
    component: <NewsPage/>
  },
  {
    to: '/news',
    component: <NewsPage/>
  },
  {
    to: '/regions',
    component: <RegionsPage/>
  },
  {
    to: '/video',
    component: <VideoPage/>
  },
  {
    to: '/tv',
    component: <TvPage/>
  }
];
