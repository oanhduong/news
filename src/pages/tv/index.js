import React, { Component } from 'react';
import Navbar from '../../components/navbar';
import Footer from '../../components/footer';
import Newslist from '../../components/newslist';

class TvPage extends Component {
  render() {
    return (
      <div>
        <Navbar title="TV"/>
        <Newslist />
        <Footer />
      </div>
    );
  }
}

export default TvPage;