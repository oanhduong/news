import React, { Component } from 'react';
import Navbar from '../../components/navbar';
import Footer from '../../components/footer';
import Newslist from '../../components/newslist';

class VideoPage extends Component {
  render() {
    return (
      <div>
        <Navbar title="Video"/>
        <Newslist />
        <Footer />
      </div>
    );
  }
}

export default VideoPage;