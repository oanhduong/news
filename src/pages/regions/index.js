import React, { Component } from 'react';
import Navbar from '../../components/navbar';
import Footer from '../../components/footer';
import Newslist from '../../components/newslist';

class RegionsPage extends Component {
  render() {
    return (
      <div>
        <Navbar title="Regions"/>
        <Newslist />
        <Footer />
      </div>
    );
  }
}

export default RegionsPage;