const initState = {
  page: 1,
  pageSize: 15,
  news: [],
  search: undefined,
  loading: false,
};

export const NewsActionTypes = {
  LOAD_NEWS: 'LOAD_NEWS',
  APPEND_NEWS: 'APPEND_NEWS',
};

export const appendNewsFn = (payload) => ({
  type: NewsActionTypes.APPEND_NEWS,
  payload,
});

export const loadNewsFn = (payload) => ({
  type: NewsActionTypes.LOAD_NEWS,
  payload,
});

const reducer = (state = initState, {type, payload}) => {
  switch(type) {
    case NewsActionTypes.LOAD_NEWS: 
      return {...state, loading: true};
    case NewsActionTypes.APPEND_NEWS:
      // eslint-disable-next-line no-case-declarations
      const news = payload.search !== state.search ? payload.news : [...state.news, ...payload.news];
      return {
        page: payload.page,
        pageSize: payload.pageSize,
        news,
        search: payload.search,
        loading: false,
      };
    default:
      return state;
  }
};

export default reducer;

