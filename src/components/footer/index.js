import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './style.css';
class Footer extends Component {
  render() {
    return (
      <div className="footer d-none d-md-block search-box">
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="navbar-collapse" id="navbarText">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item active">
                <Link className="nav-link" to="/news">News</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/regions">Regions</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/video">Video</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/tv">TV</Link>
              </li>
            </ul>
            <small className="text-muted">Copyright @AMPOS</small>
          </div>
        </nav>
      </div>
    );
  }
}

export default Footer;