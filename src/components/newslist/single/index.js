import React, { memo } from 'react';
import './style.css';
import { miliToDate } from '../../util';
import {List} from 'react-content-loader';

const singleNewsFunc = (props) => {
  const { news } = props;
  if (news) {
    return (
      <div className="card">
        <div className="card-body">
          <h5 className="card-title fix-25">{news.title}</h5>
          <img src={news.image} className="card-img-top" alt="News's background" />
          <p className="card-text fix-125">{news.desc}</p>
          <p className="card-text"><small className="text-muted">Updated: {miliToDate(news.updatedAt)}</small></p>
        </div>
      </div>
    );
  }
  return <div className="card"><List /></div>; 
};
const SingleNews = memo(singleNewsFunc);
export default SingleNews;
