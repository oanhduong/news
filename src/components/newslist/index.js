import './style.css';
import React, { Component } from 'react';
import SingleNews from './single';
import PropTypes from 'prop-types';
import {loadNewsFn} from '../../reducers/news';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';

class Newslist extends Component {

  constructor(props) {
    super(props);
    if (this.props.news.length === 0) {
      this.props.loadNews({page: 1, pageSize: this.props.pageSize});
    }
    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
    const {loadNews, page, pageSize} = this.props;
    loadNews({page, pageSize});
  }

  render() {
    let { news, loading } = this.props;
    news = news.length > 0 && !loading ? news : Array(15).fill(null);
    return (
      <div className="newslist container">
        <div className="row">
          {news.map((item, index) => {
            return (
              <div key={index} className="col-md-4">
                <SingleNews news={item} />
              </div>
            );
          })}
          
          <button type="button" className="btn btn-outline-secondary btn-block load-more" onClick={this.loadMore}>Load more</button>
        </div>
      </div>
    );
  }
}

Newslist.propTypes = {
  page: PropTypes.number,
  pageSize: PropTypes.number,
  news: PropTypes.array,
  loadNews: PropTypes.func
};

const mapStateToProps = (state) => ({...state.NewsReducer});
const mapDispatchToProps = (dispatch) => ({
  loadNews: bindActionCreators(loadNewsFn, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Newslist);