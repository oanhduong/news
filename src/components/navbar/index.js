import './style.css';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import {loadNewsFn} from '../../reducers/news';
import { connect } from 'react-redux';

class Navbar extends Component {

  constructor(props) {
    super(props);
    this.togglerButton = React.createRef();
    this.handleMenuClick = this.handleMenuClick.bind(this);
    this.search = this.search.bind(this);
    this.state = {showTitle: true};
  }

  handleMenuClick() {
    this.setState({showTitle: this.togglerButton.current.className.includes('collapsed')});
  }

  search(event) {
    if (event.key === 'Enter') {
      const {loadNews} = this.props;
      loadNews({page: 1, search: event.target.value});
    }
  }

  render() {
    const { title } = this.props;
    return (
      <div className="header">
        <nav className="navbar navbar-expand-md p-0">
          <div className="navbar-brand bg-white m-0 p-0">Logo</div>

          {this.state.showTitle ? <span className="title">{title}</span> : null}

          <button onClick={this.handleMenuClick} ref={this.togglerButton} className="navbar-toggler" type="button"
            data-toggle="collapse" data-target="#content" aria-controls="content" aria-expanded="false">
            <i className="fas fa-bars"></i>
          </button>

          <div className="collapse navbar-collapse" id="content">
            <ul className="navbar-nav">
              <li className="nav-item"><Link to="/news">News</Link></li>
              <li className="nav-item"><Link to="/regions">Regions</Link></li>
              <li className="nav-item"><Link to="/video">Video</Link></li>
              <li className="nav-item"><Link to="/tv">TV</Link></li>
            </ul>
            <div className="input-group desktop">
              <div className="input-group-prepend m-0">
                <span className="input-group-text" id="basic-addon1">
                  <i className="fas fa-search"></i>
                </span>
              </div>
              <input onKeyPress={this.search} type="text" className="form-control p-0" placeholder="Search"></input>
            </div>
          </div>
        </nav>
        <div className="input-group mobile">
          <div className="input-group-prepend m-0">
            <span className="input-group-text" id="basic-addon1">
              <i className="fas fa-search"></i>
            </span>
          </div>
          <input onKeyPress={this.search} type="text" className="form-control p-0" placeholder="Search"></input>
        </div>
      </div>
    );
  }
}

Navbar.propTypes = {
  title: PropTypes.string,
  page: PropTypes.number,
  search: PropTypes.string,
  loadNews: PropTypes.func,
};

const mapDispatchToProps = (dispatch) => ({
  loadNews: bindActionCreators(loadNewsFn, dispatch)
});

export default connect(null, mapDispatchToProps)(Navbar);

