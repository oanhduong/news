export const miliToDate = (time) => {
  const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
  ];
  const date = new Date(parseInt(time));
  return `${date.getDate()} ${monthNames[date.getMonth()]}, ${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`;
};

