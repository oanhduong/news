import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router';
import {default as NewsReducer} from './reducers/news';
import {default as NewsSaga} from './saga/news';
import {all} from 'redux-saga/effects';

export const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  NewsReducer
});

export const createRootSaga = function* () {
  yield all([
    NewsSaga()
  ]);
};
